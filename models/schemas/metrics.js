/*
    Metric schema
*/
var mongoose = require('mongoose');

var MetricSchema = module.exports = new mongoose.Schema({
  _id: {
    type: Number,
    required: true
  },
  biomet: {
    bf: [String],
    bw: [String]
    //fm: String,  -- bw*bf/100
    //lm: String -- bw - fm
  },
  power: {
    vj: [String],
      // watts: String -- (61.9*(vj*2.54))+(36*(bw/2.2))+1822
      // centimeters v*2.54
    lj: [String],
      // watts: String -- (61.9*(lj*2.54))+(36*(bw/2.2))+1822
      // centimeters v*2.54
    fjump: {
      ht: [String],
      gct: [String]
    }
    // eratio: {type: String, get: } -- vj.v / fjump.ht
  },
  strength: {
    pcl: [String],  // if no String then 
    bsq: [String],
    bp: [String],
    pups: [String]
    // pcl_bw: String, -- pcl / (bw/2.2) -- account for: inj
    // bsq_bw: String, -- bsq / (bw/2.2) -- account for: inj
    // bp_bw: String   -- bp / (bw/2.2) -- account for: inj
  },
  speed: {
    tenyds: [String],
    twnyds: [String],
    fvmlbk: [String]
  },
  date: {
    sqID: {type: Number, required: true},
    date: {type: String, required: true}
  }
},
  {
    collection: 'metrics',
    safe: true
  }
);

// methods

// indexes
MetricSchema.index({_id: 1});
